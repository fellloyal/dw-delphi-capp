object frmmain: Tfrmmain
  Left = 356
  Top = 185
  BorderIcons = [biSystemMenu, biMinimize]
  BorderStyle = bsSingle
  Caption = #22312#32447#33258#21160#26356#26032#31243#24207
  ClientHeight = 327
  ClientWidth = 551
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = True
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object lbl2: TUpLabel
    Left = 100
    Top = 10
    Width = 63
    Height = 13
    AutoSize = False
    Caption = 'lblshengjxh'
  end
  object p1: TUPanel
    Left = 0
    Top = 0
    Width = 551
    Height = 36
    Align = alTop
    ParentColor = True
    TabOrder = 0
    object lbl1: TUpLabel
      Left = 7
      Top = 11
      Width = 84
      Height = 13
      Caption = #26356#26032#21319#32423#24207#21495#65306
    end
    object lblshengjxh: TUpLabel
      Left = 98
      Top = 10
      Width = 293
      Height = 15
      AutoSize = False
      Caption = 'lblshengjxh'
    end
    object btnbegin: TUpSpeedButton
      Left = 454
      Top = 7
      Width = 85
      Height = 24
      Caption = #24320#22987#26356#26032
      Flat = True
      Glyph.Data = {
        36030000424D3603000000000000360000002800000010000000100000000100
        18000000000000030000120B0000120B00000000000000000000FF00FF0274AC
        0274AC0274AC0274AC0274AC0274AC0274AC0274AC0274AC0274AC0274AC0274
        AC0274ACFF00FFFF00FF8E5D598E5D598E5D598E5D598E5D598E5D597342414A
        BFF74ABFF64ABFF74ABFF64BC0F72398CC0274ACFF00FFFF00FF96645CFFFEDC
        FFE7BDFFE2B1FFDAA4FFEFA6265B1D046B0B046B0B046B0B37ABA353C7F7279D
        CE6ACBE50274ACFF00FFA16E5FFFFBEEFFE3CAFFDDBCFFD4ADFFE6AD7342414C
        C0CA046B0B088013046B0B32A4822AA0C799EDF70274ACFF00FFAF7965FFFBF8
        FFEBD7FEE6CAFFDCBAFFEEBC73424169DCFB6ADCFB046B0B14A428046B0B2092
        8A9FF0F70274ACFF00FFBD866AFFFEFEFFF4E7FEEDD9FEE3C7FFFAD073424174
        E5FC74E5FC42AF91046B0B21B43E046B0B97EBE552BBD70274ACCC926FFFFFFF
        FEF7F0FFF6E9FFF4E2FFFFE673424197DCCF88D0BB649162046B0B35D05D046B
        0B7AA37A86A78F0274ACD89E74FFFFFFFFFCFAFCF4EBDCC2B8BCA5A973424102
        6D66046B0B046B0B2EC2533FDA6E2AB64C046B0B046B0B0274ACE1A577FEFBFA
        FEFBFAEAE0E0A0675BA0675BD49F6083F2FE48B58E046B0B42E17548E98033C2
        5A046B0BEDDDA1734241E1A577D1926DD1926DD1926DA0675BC2B1708FE9E789
        F8FE8AFAFE5D6A37046B0B2DBC53046B0BEBD3ABFFEEBC734241FF00FF0274AC
        FEFEFE8FFEFF8FFEFF8FFEFF0274AC0274AC0274ACBD866A6AA76C046B0BE6E0
        C5FEE3C7FFFAD0734241FF00FFFF00FF0274AC0274AC0274AC0274ACFF00FFFF
        00FFFF00FFCC926FFFFFFFFEF7F0FFF6E9FFF4E2FFFFE6734241FF00FFFF00FF
        FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFD89E74FFFFFFFFFCFAFCF4
        EBDCC2B8BCA5A9734241FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
        00FFFF00FFE1A577FEFBFAFEFBFAEAE0E0A0675BA0675BD49F60FF00FFFF00FF
        FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFE1A577D1926DD1926DD192
        6DA0675BD49F60FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
        00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF}
      OnClick = btnbeginClick
      alignleftorder = 0
      alignrightorder = 0
    end
  end
  object lstfile: TUpListBox
    Left = 0
    Top = 36
    Width = 551
    Height = 272
    Align = alClient
    ImeName = #20013#25991' ('#31616#20307') - '#25628#29399#25340#38899#36755#20837#27861
    ItemHeight = 13
    TabOrder = 1
    AutoInit = False
    HistoryValue = -1
    HistoryValueOnOff = True
  end
  object stb: TStatusBar
    Left = 0
    Top = 308
    Width = 551
    Height = 19
    Panels = <>
  end
  object adoconn: TUpAdoConnection
    LoginPrompt = False
    Provider = 'SQLOLEDB.1'
    Left = 120
    Top = 152
  end
  object qry: TUpAdoQuery
    Connection = adoconn
    Parameters = <>
    Left = 160
    Top = 152
  end
  object qryfile: TUpAdoQuery
    Connection = adoconn
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'SELECT * FROM xt_shengjwjqd')
    Left = 192
    Top = 152
    object atncfldqryfilebianm: TAutoIncField
      FieldName = 'bianm'
      ReadOnly = True
    end
    object intgrfldqryfileshengjxh: TIntegerField
      FieldName = 'shengjxh'
    end
    object strngfldlxmcqryfileshiykhbm: TStringField
      FieldName = 'shiykhbm'
      Size = 32
    end
    object strngfldlxmcqryfilewenjmc: TStringField
      FieldName = 'wenjmc'
      Size = 120
    end
    object intgrfldqryfilewenjdx: TIntegerField
      FieldName = 'wenjdx'
    end
    object strngfldlxmcqryfilewenjccml: TStringField
      FieldName = 'wenjccml'
      Size = 120
    end
    object dbfieldqryfilewenj: TBlobField
      FieldName = 'wenj'
    end
    object intgrfldqryfileshifljyx: TIntegerField
      FieldName = 'shifljyx'
    end
    object intgrfldqryfilexiazcs: TIntegerField
      FieldName = 'xiazcs'
    end
  end
  object qry1: TUpAdoQuery
    Connection = adoconn
    Parameters = <>
    Left = 224
    Top = 152
  end
end
