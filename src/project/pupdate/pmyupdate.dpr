program pmyupdate;

uses
  Forms,
  Windows,
  Messages,
  CnHint,
  SysUtils,         
  ucsmsg in '..\classes\csmsg\ucsmsg.pas',
  ufrmmain in 'ufrmmain.pas' {frmmain},
  ubase in '..\lib\common\ubase.pas',
  udbbase in '..\lib\common\udbbase.pas',
  udbconfigfile in '..\lib\common\udbconfigfile.pas',
  uencode in '..\lib\common\uencode.pas',
  ukeyvaluelist in '..\lib\common\ukeyvaluelist.pas',
  umyconfig in '..\lib\common\umyconfig.pas',
  utypes in '..\lib\common\utypes.pas',      
  utblyuang in '..\classes\dbtableclass\utblyuang.pas',
  HzSpell in '..\lib\moudle\huoqupinyin\HzSpell.pas',
  ufrmcomm_showmessage in '..\dllmoudle\common\ufrmcomm_showmessage.pas' {frmcomm_showmessage},
  ulog in '..\lib\common\ulog.pas',
  ufrmcomm_showdialog in '..\dllmoudle\common\ufrmcomm_showdialog.pas' {frmcomm_showdialog},
  ufrmbase in '..\lib\formbase\ufrmbase.pas' {frmbase},
  uglobal in '..\lib\common\uglobal.pas',
  umysystem in '..\lib\common\umysystem.pas';

{$R *.res}

begin
  try
    Application.Initialize; 
    Application.CreateForm(Tfrmmain, frmmain);
    Application.CreateForm(Tfrmcomm_showdialog, frmcomm_showdialog);
    Application.CreateForm(Tfrmcomm_showmessage, frmcomm_showmessage);
    log := TLog.Create('AS_update_');

    if ParamCount <> 6 then
    begin
      raise Exception.Create('非法的参数调用!');
    end;
                
    frmmain.fshengjxh_local := ParamStr(1);
    frmmain.fshengjxh := ParamStr(2);
    frmmain.fdbserver := ParamStr(3);
    frmmain.fdbuser := ParamStr(4);
    frmmain.fdbpassword := ParamStr(5);
    frmmain.fdbdatabase := ParamStr(6);

    log.appendlog('shengjxh_local=' + frmmain.fshengjxh_local);
    log.appendlog('shengjxh=' + frmmain.fshengjxh);
    log.appendlog('dbserver=' + frmmain.fdbserver);
    log.appendlog('dbuser=' + frmmain.fdbuser);
    log.appendlog('dbdatabase=' + frmmain.fdbdatabase);
    if not frmmain.init() then
    begin
      raise Exception.Create('更新程序初始化失败:' + frmmain.errmsg);
    end;

    Application.Run;
  except
    on E:Exception do
    begin
      log.appendlog(E.Message);
      frmmain.baseobj.showerror(E.Message);

      if Assigned(log) then
        FreeAndNil(log);
      if Assigned(frmmain) then
        FreeAndNil(frmmain);
        
      application.Terminate;
      exit;
    end;
  end;
end.
