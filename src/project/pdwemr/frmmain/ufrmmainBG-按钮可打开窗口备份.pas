{
  导航面板顺序说明：
    0---门诊业务
    1---住院业务
    2---医技业务
    3---药库业务
    4---药房业务
    5---物资库业务
    6---财务管理
    7---院长查询
    8---数据字典
    9---基础数据
    10---系统管理
}

unit ufrmmainBG;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, OleCtrls, SHDocVw, ExtCtrls, Buttons, UpSpeedButton, StdCtrls,
  GradientLabel, UpLabel, UPanel, RzTabs, dxGDIPlusClasses,
  umysystem, udlltransfer, udllfunctions, utypes, ufrmdbbase, frxExportXLS,
  frxClass, frxExportPDF, Menus, UpPopupMenu, frxDesgn, frxDBSet,
  UpDataSource, DB, ADODB, UpAdoTable, UpAdoQuery;

type
  TfrmmainBG = class(Tfrmdbbase)
    wb: TWebBrowser;
    pcl: TRzPageControl;
    tsmenzyw: TRzTabSheet;
    pmenz: TPanel;
    btnmenzguah: TUpSpeedButton;
    btn1: TUpSpeedButton;
    p2: TUPanel;
    lbl1: TGradientLabel;
    lbl2: TGradientLabel;
    lbl3: TUpLabel;
    btn2: TUpSpeedButton;
    btn50: TUpSpeedButton;
    btn54: TUpSpeedButton;
    btn53: TUpSpeedButton;
    btn48: TUpSpeedButton;
    btn51: TUpSpeedButton;
    btn56: TUpSpeedButton;
    btn55: TUpSpeedButton;
    procedure FormShow(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure btnmenzguahMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure pmenzMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure btnmenzguahClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private

  protected
    function Execute_dll_show_before() : Boolean; override;

  public
    ffrmmainhandle : HWND;
    //主窗体句柄

    procedure changeNav(str : string);
    //主窗体通知切换导航面板
  end;

var
  frmmainBG: TfrmmainBG;

implementation

{$R *.dfm}

{ TfrmmainBG }

procedure TfrmmainBG.FormShow(Sender: TObject);
begin
  self.WindowState := wsMaximized;
end;

procedure TfrmmainBG.FormActivate(Sender: TObject);
begin
  self.WindowState := wsMaximized;
end;

procedure TfrmmainBG.changeNav(str: string);
var
  iindex : integer;
begin
  { 主窗体frmmain发来的当前navbar信息
    切换不同的导航面板
  }

  iindex := 0;
  pcl.Visible := true;
  pcl.ActivePageIndex := iindex;
end;

procedure TfrmmainBG.FormResize(Sender: TObject);
begin
  { 窗体大小变化，调整导航面板位置水平居中
  }
  pcl.Left := (self.Width-pcl.Width) div 2;
end;

procedure TfrmmainBG.btnmenzguahMouseMove(Sender: TObject; Shift: TShiftState; X,
  Y: Integer);
begin
  { btn鼠标覆盖，变换字体颜色和样式
  }
  (sender as TUpSpeedButton).Font.Color := clBlue;
  (sender as TUpSpeedButton).Font.Style := [fsBold]
end;

procedure TfrmmainBG.pmenzMouseMove(Sender: TObject; Shift: TShiftState; X,
  Y: Integer);
var
  i : integer;
begin
  { 模拟鼠标在btn上的离开事件
    按钮字体颜色样式恢复正常
  }
  for i:=0 to self.ComponentCount-1 do
  begin
    if self.Components[i] is TUpSpeedButton then
    begin
      (self.Components[i] as TUpSpeedButton).Font.Color := clBlack;
      (self.Components[i] as TUpSpeedButton).Font.Style := [];
    end;
  end;

end;

procedure TfrmmainBG.btnmenzguahClick(Sender: TObject);
begin
  { 验证用户权限
  if not dmtbl_public.yisHasMenuitemQX(dllparams.mysystem^.loginyuang.jiaosbm,
      (sender as TUpSpeedButton).Tag) then
  begin
    baseobj.showerror('对不起，您没有此项业务权限！');
    Exit;
  end;  
  }

  { 进入业务处理
    调用frmmain窗体菜单项单击事件统一处理dll接口；
    传入菜单id以寻址对应的业务处理函数；
    菜单id在配置sql中已经分配给定；菜单项的显示顺序由listorder字段单独控制；
  }
  frmmain_menuitem_click(@dllparams, (sender as TUpSpeedButton).Tag);

  //业务窗体启动后关闭导航栏
  SendMessage(ffrmmainhandle, UPM_FRMMAIN_LEFTNAVBAR_CLOSE, 0, 0);
end;

function TfrmmainBG.Execute_dll_show_before: Boolean;
begin
  result := inherited Execute_dll_showmodal_before();

  wb.Navigate('file:///' + ExtractFilePath(Application.ExeName) + 'web/index.html');
end;

procedure TfrmmainBG.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  inherited;

  frmmainBG := nil;
end;

end.
