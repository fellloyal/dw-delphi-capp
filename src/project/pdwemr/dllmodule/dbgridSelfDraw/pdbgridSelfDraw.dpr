library pdbgridSelfDraw;

{ Important note about DLL memory management: ShareMem must be the
  first unit in your library's USES clause AND your project's (select
  Project-View Source) USES clause if your DLL exports any procedures or
  functions that pass strings as parameters or function results. This
  applies to all strings passed to and from your DLL--even those that
  are nested in records and classes. ShareMem is the interface unit to
  the BORLNDMM.DLL shared memory manager, which must be deployed along
  with your DLL. To avoid using BORLNDMM.DLL, pass string information
  using PChar or ShortString parameters. }

uses
  SysUtils,
  Classes,
  Graphics,
  Forms,
  DB,
  Dialogs,
  ADODB,
  Grids,
  Messages,
  Windows,
  Wwdbigrd,
  Wwdbgrid,
  UpWWDbGrid,
  udbgselfdraw_xitgl in 'udbgselfdraw_xitgl.pas',
  udbgselfdraw_dbgid_define in 'udbgselfdraw_dbgid_define.pas',
  udbgCalcCellColorDispath in 'udbgCalcCellColorDispath.pas',
  udbgselfdraw_zhuyyisz in 'udbgselfdraw_zhuyyisz.pas',
  udbgselfdraw_menzyisz in 'udbgselfdraw_menzyisz.pas',
  udbgselfdraw_jichushuju in 'udbgselfdraw_jichushuju.pas',
  udbgselfdraw_zhuyhusz in 'udbgselfdraw_zhuyhusz.pas',
  udbgselfdraw_cxbb_yaok in 'udbgselfdraw_cxbb_yaok.pas',
  udbgselfdraw_cxbb_yaof in 'udbgselfdraw_cxbb_yaof.pas',
  udbgselfdraw_cxbb_wuzk in 'udbgselfdraw_cxbb_wuzk.pas',
  udbgselfdraw_iyblz_dongr in 'udbgselfdraw_iyblz_dongr.pas';

type
  TDbgCalcCellColorFun = procedure(Sender: TObject; Field: TField; State: TGridDrawState;
  Highlight: Boolean; AFont: TFont; ABrush: TBrush) of object;

{$R *.res}

var
  { dll的Application和screen临时存储,在dll卸载的时候释放资源
  }
  DLLApp: TApplication;
  DLLScreen: TScreen;
       
{ dll卸载时的资源恢复和释放
}
procedure DLLUnloadProc(Reason: Integer);
begin
  if Reason = 0 then //DLL_PROCESS_DETACH
  begin
    Application := DLLApp; //恢复
    Screen := DLLScreen;
    //关闭dll
    SendMessage(Application.Handle, WM_CLOSE, 0, 0);
    FreeLibrary(Application.Handle);
  end;
end;

{ 开始业务函数
}
//OnCalcCellColors事件入口函数
procedure CalcCellColors_main(Sender: TObject; Field: TField; State: TGridDrawState;
  Highlight: Boolean; AFont: TFont; ABrush: TBrush); stdcall;
begin
  { 通用的处理部分 }
  //dbg是否进入只读状态
  if (Sender as TwwDBGrid).ReadOnly then
  begin
    //是否高亮选中
    if Highlight then
    begin
      ABrush.Color := StringToColor(dbg_highlight_color);
      AFont.Color := clBlue;
      //AFont.Style := [fsBold];
    end
    else
    begin
      ABrush.Color := StringToColor(dbg_readonly_color);
      AFont.Color := clBlack;
      AFont.Style := [];
    end;
  end
  else
  begin
    //字段只读
    if (Sender as TwwDBGrid).ColumnByName(Field.FieldName).ReadOnly then
    begin
      //高亮显示
      if Highlight then
      begin
        ABrush.Color := StringToColor(dbg_highlight_color);
        AFont.Color := clBlue;
        //AFont.Style := [fsBold];
      end
      else
      //不是高亮显示
      begin                      
        ABrush.Color := StringToColor(dbg_field_readonly_color);
        AFont.Color := clBlack;
        AFont.Style := [];
      end;
    end
    else
    //字段不是只读
    begin
      //高亮显示
      if Highlight then
      begin                    
        ABrush.Color := StringToColor(dbg_highlight_color);
        AFont.Color := clBlue;
        //AFont.Style := [fsBold];
      end
      else
      //不是高亮显示
      begin                  
        ABrush.Color := StringToColor(dbg_edit_color);
        AFont.Color := clBlack;
        AFont.Style := [];
      end;
    end;
  end;

  if Assigned(Field) then
  begin 
    //金额字段统一处理；
    if ((LowerCase(Field.FieldName)='chengbdj') or
        (LowerCase(Field.FieldName)='lingsdj') or
        (LowerCase(Field.FieldName)='chengbjje') or
        (LowerCase(Field.FieldName)='lingsjje') or    
        (LowerCase(Field.FieldName)='jine') or
        (LowerCase(Field.FieldName)='zongje_chengbj') or
        (LowerCase(Field.FieldName)='zongje_lingsj') or
        (LowerCase(Field.FieldName)='jincce') or
        (LowerCase(Field.FieldName)='jinxce')) then
    begin
      AFont.Color := StringToColor(dbg_moneyfield_forcecolor);
    end;
         
    //数量字段统一处理
    if ((LowerCase(Field.FieldName)='shul') or
        (LowerCase(Field.FieldName)='baozsl')) then
    begin
      AFont.Color := StringToColor(dbg_numberfield_forcecolor);
    end;
  end;

  { 各业务窗体各自DbGrid个性化处理部分 }
  dbgCalcCellColorDispath(Sender, Field, State, Highlight, AFont, ABrush);
end;

exports
  CalcCellColors_main;

begin      
  { 将dll的Application和Screen存储下来,在卸载的时候恢复
  }                        

  DLLApp := Application;
  DLLScreen := Screen;
  //dll相关函数处理,这里处理卸载时的资源释放
  DLLProc := @DLLUnloadProc;
end.
