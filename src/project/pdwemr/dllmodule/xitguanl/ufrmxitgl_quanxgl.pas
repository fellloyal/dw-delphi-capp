unit ufrmxitgl_quanxgl;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ufrmdbnavbase, Menus, UpPopupMenu, frxClass, frxDesgn, frxDBSet,
  UpDataSource, DB, ADODB, UpAdoTable, UpAdoQuery, ExtCtrls, Buttons,
  UpSpeedButton, UPanel, ComCtrls, UpPageControl, Grids, Wwdbigrd,
  Wwdbgrid, UpWWDbGrid, StdCtrls, UpListBox, UpComboBox, UpLabel,
  UpCheckBox, frxExportXLS, frxExportPDF, UpMemo, UpEdit, DBGrids, UpDbGrid,
  RzTreeVw, RzPanel, RzRadGrp, RzSplit, RzLstBox, RzChkLst, RzButton, utypes,
  RzTabs, udmtbl_jiaosdy, ImgList, ToolPanels, UpAdvToolPanel,UpAdoStoreProc;

const
  clRztreeSelectedColor = $00FFEEDD;
  clRztreeSelectedChar = '■';
  
type                     
  PMenuItemNode = ^TMenuItemNode;
  TMenuItemNode = class
    bianm : Integer;       
  end;

  PTabSheet = ^TTabSheet;

  Tfrmxitgl_quanxgl = class(Tfrmdbnavbase)
    qryjiaosdy: TUpAdoQuery;
    dsjiaosdy: TDataSource;
    qryjiaosyg: TUpAdoQuery;
    dsjiaosyg: TDataSource;
    pg1: TRzPageControl;
    tsTabSheet1: TRzTabSheet;
    tsTabSheet2: TRzTabSheet;
    pupl6: TUPanel;
    btnjiaosqx: TUpSpeedButton;
    pupl2: TUPanel;
    upl_jiaosqx_set_title: TUPanel;
    lstjiaosdy: TUpListBox;
    upc2: TUpPageControl;
    tsjicsj: TTabSheet;
    tsxitgl: TTabSheet;
    rzp10: TRzGroupBox;
    rztreexitgl: TRzCheckTree;
    pupl1: TUPanel;
    lbl2: TUpLabel;
    lbl3: TUpLabel;
    pupl4: TUPanel;
    btn1: TUpSpeedButton;
    btn2: TUpSpeedButton;
    btn4: TUpSpeedButton;
    edtjiaosmc: TUpEdit;
    mmojiaosms: TUpMemo;
    dbg1: TUpWWDbGrid;
    dbgjiaosyg: TUpWWDbGrid;
    rzp16: TRzGroupBox;
    rztreejicsj: TRzCheckTree;
    tsxuexzx: TTabSheet;
    rzp1: TRzGroupBox;
    rztreexuexzx: TRzCheckTree;
    upmnSelyuang_jiaosdy: TUpPopupMenu;
    NJiaoscy_add: TMenuItem;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btn1Click(Sender: TObject);
    procedure btn2Click(Sender: TObject);
    procedure btn4Click(Sender: TObject);
    procedure lstjiaosdyDblClick(Sender: TObject);
    procedure btnjiaosqxClick(Sender: TObject);
    procedure tsjicsjExit(Sender: TObject);
    procedure tsxitglExit(Sender: TObject);
    procedure qryjiaosdyAfterScroll(DataSet: TDataSet);
    procedure NJiaoscy_addClick(Sender: TObject);
  private
    strsql : string;

    dmjiaosdy : Tdmjiaosdy;
    //角色定义表对象

    { 新版本角色授权增加
    }
    procedure refreshdata_jiaosdy();
    //刷新角色定义表
    procedure refreshlist_jiaosdy();
    //刷新角色lst,设置角色权限时角色列表
    procedure refreshdata_jiaosyuang(jiaosbm: integer);
    //刷新角色下员工
    procedure addYuangToJiaos(yuangbm: integer);
    //添加员工到角色组
    
    procedure jiaosqx_init(); overload;
    //角色权限初始化
    procedure jiaosqx_init(moksy : integer; rztree : PRzCheckTree); overload;
    //加载指定索引模块的菜单

    procedure jiaosqx_reset(); overload;
    //角色权限重置；全部未选中；
    procedure jiaosqx_reset(rztree : PRzCheckTree); overload;

    function jiaosqx_load(jiaosbm : Integer) : Integer; overload;
    //加载指定角色编码的权限
    function jiaosqx_load(jiaosbm : Integer; rztree : PRzCheckTree) : Integer; overload;

    procedure jiaosqx_save(jiaosbm : Integer); overload;
    //保存角色权限
    procedure jiaosqx_save(jiaosbm : Integer; rztree : PRzCheckTree); overload;

    function jiaosqx_rztreeHasSelectedItem(rztree : PRzCheckTree) : Boolean;
    //rztree是否有一个item选中   

    procedure jiaosqx_onTabSheetExit(tabsheet : PTabSheet; tabsheetCaption : string;
        rztree1, rztree2, rztree3, rztree4, rztree5 : PRzCheckTree);
    //在各个模块的tabsheet离开的时候判断是否有菜单选中，进而改变显示颜色和caption

    procedure jiaosqx_freeData(); overload;
    //销毁权限tree的data指针
    procedure jiaosqx_freeData(rztree : PRzCheckTree); overload;

    procedure jiaosqx_pg_ts_caption_reset(); overload;
    //权限设置 ts页面caption重置
  protected
    function Execute_dll_show_before() : Boolean; override;
  end;

var
  frmxitgl_quanxgl: Tfrmxitgl_quanxgl;

implementation

{$R *.dfm}

function Tfrmxitgl_quanxgl.Execute_dll_show_before: Boolean;
var
  i : integer;
begin
  inherited Execute_dll_show_before();
  
  { 新版导航栏
  }
  //创建角色定义表对象
  dmjiaosdy := Tdmjiaosdy.Create(@dllparams);
  
  //刷新角色定义表 for dbg
  refreshdata_jiaosdy;
  //刷新角色定义list
  refreshlist_jiaosdy;
    
  //选择导航栏
  jiaosqx_init;

  //导航栏权限checklist控件高度修正，这个是空间bug导致，手工处理一下
  for i:=0 to self.ComponentCount-1 do
  begin
    if self.Components[i] is TRzCheckTree then
    begin
      (self.Components[i] as TRzCheckTree).Height :=
        (self.Components[i] as TRzCheckTree).Parent.Height;
    end;
  end;
  
  Result := True;
end;

procedure Tfrmxitgl_quanxgl.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;

  //销毁权限tree的data指针
  jiaosqx_freeData;

  frmxitgl_quanxgl := nil;
end;


procedure Tfrmxitgl_quanxgl.jiaosqx_init;
begin
  { 系统管理
  }
  jiaosqx_init(0, @rztreexitgl);

  { 基础数据
  }
  jiaosqx_init(1, @rztreejicsj);

  { 评审管理
  }
  jiaosqx_init(3, @rztreexuexzx);
end;

procedure Tfrmxitgl_quanxgl.jiaosqx_init(moksy: integer; rztree : PRzCheckTree);
var
  RootNode, ParentNode1, ParentNode2, Node: TTreeNode;  
  itemnode : TMenuItemNode;
begin
  rztree.Items.Clear;
  
  qry.Close;
  qry.SQL.Text := 'select * from sys_caiddy ' +
    ' where moksy=' + IntToStr(moksy) +
    ' order by moksy,zusy,annsy';
  qry.Open;
  if qry.RecordCount<=0 then Exit;

  //新增根节点
  ParentNode1 := nil;
  ParentNode2 := nil;

  while not qry.Eof do
  begin
    if ParentNode1=nil then
    begin
      //创建一级节点
      ParentNode1 := rztree^.Items.AddChild(RootNode, qry.getString('mokmc'));

      //创建二级节点
      ParentNode2 := rztree^.Items.AddChild(ParentNode1, qry.getString('zubt'));
      
      //创建三级菜单
      node := rztree^.Items.AddChild(ParentNode2, qry.getString('annwb')); 
      itemnode := TMenuItemNode.Create;
      itemnode.bianm := qry.fieldbyname('bianm').AsInteger;
      node.Data := itemnode;
    end
    else
    begin    
      if SameText(LowerCase(ParentNode1.Text), LowerCase(qry.fieldbyname('mokmc').AsString)) then
      begin
        if not SameText(LowerCase(ParentNode2.Text), LowerCase(qry.fieldbyname('zubt').AsString)) then
        begin
          ParentNode2 := rztree^.Items.AddChild(ParentNode1, qry.fieldbyname('zubt').AsString);
        end;

      end
      else
      begin
        ParentNode1 := rztree^.Items.AddChild(RootNode, qry.fieldbyname('mokmc').AsString);
        ParentNode2 := rztree^.Items.AddChild(ParentNode1, qry.fieldbyname('zubt').AsString);
      end;
      
      node := rztree^.Items.AddChild(ParentNode2, qry.fieldbyname('annwb').AsString);  
      itemnode := TMenuItemNode.Create;
      itemnode.bianm := qry.fieldbyname('bianm').AsInteger;
      node.Data := itemnode;
    end;

    qry.Next;
  end;
  qry.Close;

  rztree^.Items[0].Expand(True);
end;

procedure Tfrmxitgl_quanxgl.refreshdata_jiaosdy;
begin
  qryjiaosdy.DisableControls;
  qryjiaosdy.Close;
  qryjiaosdy.Open;
  qryjiaosdy.EnableControls;
end;

procedure Tfrmxitgl_quanxgl.refreshlist_jiaosdy;
begin
  qry1.OpenSql('select * from sys_jiaosdy');
  qry1.First;
  lstjiaosdy.ClearItems;
  while not qry1.Eof do
  begin
    lstjiaosdy.AddItemUP(qry1.getString('mingc'), qry1.getString('bianm'));
    qry1.Next;
  end;
  qry1.Close;
end;

procedure Tfrmxitgl_quanxgl.btn1Click(Sender: TObject);
begin
  //inherited;

  try        
    //添加前检查
    if Trim(edtjiaosmc.Text) = '' then
    begin
      baseobj.showwarning('请输入角色名称！');
      edtjiaosmc.SetFocus;
      Exit;
    end;

    strsql := format('insert into sys_jiaosdy(mingc,miaos) ' +
      ' values(''%s'',''%s'')', [edtjiaosmc.Text, mmojiaosms.Text]);
    qry1.ExecuteSql(strsql);

    //添加后处理
    //刷新角色表格
    refreshdata_jiaosdy;
    //刷新角色列表
    refreshlist_jiaosdy;

    baseobj.showmessage('角色新增成功！');
  except
    on E:Exception do
    begin      
      baseobj.showerror('新增角色发生错误：' + e.Message);
    end;
  end;
end;

procedure Tfrmxitgl_quanxgl.btn2Click(Sender: TObject);
begin
  inherited;

  try
  
    //修改前检查
    if qryjiaosdy.isEmpty then exit;
    if qryjiaosdy.getInteger('bianm') = 1 then
    begin
      baseobj.showerror('系统管理员组不允许修改！');
      exit;
    end;
    if not baseobj.showdialog('系统提示','确认修改当前角色吗？') then
    begin
      Exit;
    end;

    if Trim(edtjiaosmc.Text) = '' then
    begin
      baseobj.showwarning('请输入角色名称！');
      edtjiaosmc.SetFocus;
      Exit;
    end;

    //修改
    strsql := format('update sys_jiaosdy set mingc=''%s'',miaos=''%s''' +
      ' where bianm=%s', [edtjiaosmc.Text,mmojiaosms.Text,qryjiaosdy.getString('bianm')]);
    qry1.ExecuteSql(strsql);

    //修改后处理
    //刷新角色表格
    refreshdata_jiaosdy;
    //刷新角色列表
    refreshlist_jiaosdy;

    baseobj.showmessage('角色修改成功！');
  except
    on E:Exception do
    begin      
      baseobj.showerror('修改角色发生错误：' + e.Message);
    end;
  end;
end;

procedure Tfrmxitgl_quanxgl.btn4Click(Sender: TObject);
begin
  inherited;

  try
    //删除前检查
    if qryjiaosdy.isEmpty then exit;
    //$$$1和2的角色预留；不允许删除
    if qryjiaosdy.getInteger('bianm') < 3 then
    begin
      baseobj.showerror('系统预留角色不允许删除！');
      exit;
    end;
    if not baseobj.showdialog('系统提示','确认删除当前角色吗？') then exit;

    //删除
    strsql := format('delete from sys_jiaosdy where bianm=%s',
        [qryjiaosdy.getString('bianm')]);
    qry1.ExecuteSql(strsql);
    
    //修改后处理
    //刷新角色表格
    refreshdata_jiaosdy;
    //刷新角色列表
    refreshlist_jiaosdy;

    baseobj.showmessage('角色删除成功！');
  except
    on E:Exception do
    begin      
      baseobj.showerror('删除角色发生错误：' + e.Message);
    end;
  end;
end;

procedure Tfrmxitgl_quanxgl.lstjiaosdyDblClick(Sender: TObject);
begin
  inherited;

  if lstjiaosdy.ItemIndex = -1 then Exit;

  upl_jiaosqx_set_title.Caption := ' 角色【' + lstjiaosdy.Items[lstjiaosdy.itemindex] + '】';

  //全部置于未选中状
  jiaosqx_reset();

  //加载当前角色权限;
  jiaosqx_load(StrToInt(lstjiaosdy.GetItemValue));
end;

function Tfrmxitgl_quanxgl.jiaosqx_load(jiaosbm: Integer) : Integer;
begin
  jiaosqx_pg_ts_caption_reset;

  //系统管理
  if (jiaosqx_load(jiaosbm, @rztreexitgl) = 1) then
  begin
    tsxitgl.Caption := '系统管理' + clRztreeSelectedChar;
    rztreexitgl.Color := clRztreeSelectedColor;
  end;

  //基础数据
  if (jiaosqx_load(jiaosbm, @rztreejicsj) = 1) then
  begin
    tsjicsj.Caption := '基础数据' + clRztreeSelectedChar;
    rztreejicsj.Color := clRztreeSelectedColor;
  end;

  //评审管理
  if (jiaosqx_load(jiaosbm, @rztreexuexzx) = 1) then
  begin
    tsxuexzx.Caption := '评审管理' + clRztreeSelectedChar;
    rztreexuexzx.Color := clRztreeSelectedColor;
  end;
end;

procedure Tfrmxitgl_quanxgl.jiaosqx_reset;
begin
  tsxitgl.Caption := '系统管理';
  jiaosqx_reset(@rztreexitgl);

  tsjicsj.Caption := '基础数据';
  jiaosqx_reset(@rztreejicsj);

  tsxuexzx.Caption := '评审管理';
  jiaosqx_reset(@rztreexuexzx);
end;

procedure Tfrmxitgl_quanxgl.jiaosqx_reset(rztree: PRzCheckTree);
var
  i : Integer;
begin
  for i:=0 to rztree^.Items.Count-1 do
  begin
    rztree^.ItemState[i] := csUnchecked;
  end;
end;

procedure Tfrmxitgl_quanxgl.jiaosqx_save(jiaosbm : Integer);
begin
  try
    qry1.beginTrans;

    //删除原来的权限
    qry1.ExecuteSql('delete from sys_jiaosqx where jiaosbm=' +
      IntToStr(jiaosbm));

    //保存新权限
    jiaosqx_save(jiaosbm, @rztreexitgl);
    jiaosqx_save(jiaosbm, @rztreejicsj);
    jiaosqx_save(jiaosbm, @rztreexuexzx);

    qry1.commitTrans;
    
    baseobj.showmessage('保存角色权限成功你！');
  except
    on E:Exception do
    begin
      qry1.rollBackTrans;
      baseobj.showerror('保存角色权限失败:' + e.Message);
    end;
  end;
end;

procedure Tfrmxitgl_quanxgl.jiaosqx_save(jiaosbm : Integer; rztree: PRzCheckTree);
var
  i : Integer;
  itemnode : TMenuItemNode;
begin
  inherited;

  for i:=0 to rztree^.Items.Count-1 do
  begin
    if (rztree^.Items[i].Level = 2) and (rztree^.ItemState[i] = csChecked) then
    begin                                                                
      itemnode := TMenuItemNode(rztree^.items[i].data);

      qry2.ExecuteSql('insert into sys_jiaosqx(jiaosbm,caidbm) values(' +
        IntToStr(jiaosbm) + ',' + IntToStr(itemnode.bianm) + ')');
    end;
  end;
end;

procedure Tfrmxitgl_quanxgl.jiaosqx_freeData;
begin
  jiaosqx_freeData(@rztreexitgl);
  jiaosqx_freeData(@rztreejicsj);
  jiaosqx_freeData(@rztreexuexzx);
end;

procedure Tfrmxitgl_quanxgl.jiaosqx_freeData(rztree: PRzCheckTree);
var
  i : Integer;
  itemnode : TMenuItemNode;
begin
  inherited;

  //销毁权限tree的data指针
  for i:=0 to rztree^.Items.Count-1 do
  begin
    if rztree^.Items[i].Level = 3 then
    begin
      itemnode := TMenuItemNode(rztree^.items[i].data);
      FreeAndNil(itemnode);
    end;
  end;
  rztree^.Items.Clear;
end;

procedure Tfrmxitgl_quanxgl.btnjiaosqxClick(Sender: TObject);
begin
  inherited;

  //保存角色权限
  jiaosqx_save(StrToInt(lstjiaosdy.GetItemValue));  
end;

function Tfrmxitgl_quanxgl.jiaosqx_load(jiaosbm : Integer;
  rztree: PRzCheckTree) : Integer;
var
  i : Integer;
  itemnode : TMenuItemNode;
begin
  inherited;

  //尚未发现有权限菜单项
  Result := 0;

  //获取当前角色权限列表
  qry2.OpenSql('select * from sys_jiaosqx where jiaosbm=' + IntToStr(jiaosbm));
  
  for i:=0 to rztree^.Items.Count-1 do
  begin
    if rztree^.Items[i].Level = 2 then
    begin
      itemnode := TMenuItemNode(rztree^.items[i].data);

      //检查当前菜单编码是否在当前权限列表中
      if(qry2.Locate('caidbm', itemnode.bianm, [])) then
      begin
        rztree^.ItemState[i] := csChecked;

        //发现了有权限的菜单项
        Result := 1;
      end;      
    end;
  end;
end;

procedure Tfrmxitgl_quanxgl.jiaosqx_pg_ts_caption_reset;
begin
  tsxitgl.Caption := '系统管理';
  rztreexitgl.Color := clWindow;

  tsjicsj.Caption := '基础数据';
  rztreejicsj.Color := clWindow;

  tsxuexzx.Caption := '评审管理';
  rztreexuexzx.Color := clWindow;
end;

function Tfrmxitgl_quanxgl.jiaosqx_rztreeHasSelectedItem(
  rztree: PRzCheckTree): Boolean;
var
  i : Integer;
begin
  Result := False;
  
  for i:=0 to rztree^.Items.Count-1 do
  Begin
    if rztree^.ItemState[i] = csChecked then
    begin
      Result := True;
      Exit;
    end;
  end;
end;

procedure Tfrmxitgl_quanxgl.jiaosqx_onTabSheetExit(tabsheet: PTabSheet;
  tabsheetCaption : string;
  rztree1, rztree2, rztree3, rztree4, rztree5: PRzCheckTree);
var
  bfound : Boolean;
begin
  bfound := False;

  if rztree1 <> nil then
  begin
    if jiaosqx_rztreeHasSelectedItem(rztree1) then
    begin
      rztree1^.Color := clRztreeSelectedColor;
      bfound := True;
    end
    else
    begin
      rztree1^.Color := clWindow;
    end;
  end;

  if rztree2 <> nil then
  begin
    if jiaosqx_rztreeHasSelectedItem(rztree2) then
    begin
      rztree2^.Color := clRztreeSelectedColor;
      bfound := True;
    end
    else
    begin
      rztree2^.Color := clWindow;
    end;
  end;

  if rztree3 <> nil then
  begin
    if jiaosqx_rztreeHasSelectedItem(rztree3) then
    begin
      rztree3^.Color := clRztreeSelectedColor;
      bfound := True;
    end
    else
    begin
      rztree3^.Color := clWindow;
    end;
  end;

  if rztree4 <> nil then
  begin
    if jiaosqx_rztreeHasSelectedItem(rztree4) then
    begin
      rztree4^.Color := clRztreeSelectedColor;
      bfound := True;
    end
    else
    begin
      rztree4^.Color := clWindow;
    end;
  end;

  if rztree5 <> nil then
  begin
    if jiaosqx_rztreeHasSelectedItem(rztree5) then
    begin
      rztree5^.Color := clRztreeSelectedColor;
      bfound := True;
    end
    else
    begin
      rztree5^.Color := clWindow;
    end;
  end;

  if bfound then
  begin
    tabsheet^.Caption := tabsheetCaption + clRztreeSelectedChar;
  end
  else
  begin
    tabsheet^.Caption := tabsheetCaption;
  end;
end;

procedure Tfrmxitgl_quanxgl.tsjicsjExit(Sender: TObject);
begin
  inherited;

  jiaosqx_onTabSheetExit(@tsjicsj, '基础数据', @rztreejicsj, nil, nil, nil, nil);
end;

procedure Tfrmxitgl_quanxgl.tsxitglExit(Sender: TObject);
begin
  inherited;

  jiaosqx_onTabSheetExit(@tsxitgl, '系统管理', @rztreexitgl, nil, nil, nil, nil);
end;

procedure Tfrmxitgl_quanxgl.qryjiaosdyAfterScroll(DataSet: TDataSet);
begin
  inherited;

  if not qryjiaosdy.isEmpty then
  begin
    refreshdata_jiaosyuang(qryjiaosdy.getInteger('bianm'));
  end
  else
  begin                           
    refreshdata_jiaosyuang(0);
  end;
end;


procedure Tfrmxitgl_quanxgl.NJiaoscy_addClick(Sender: TObject);
{var
  frmcomm_selyuang: Tfrmcomm_selyuang;
  frmselyuangmutil: Tfrmselyuangmutil;
  yuangbm, jiaosbm: integer;
  }
begin
  inherited;

  { 选择员工加入，设置其角色
    刷新角色下成员

  //角色编码
  if qryjiaosdy.isEmpty then exit;
  jiaosbm:= qryjiaosdy.getInteger('bianm');

  frmselyuangmutil:= Tfrmselyuangmutil.Create(nil);
  frmselyuangmutil.FFun_SelRecord:= addYuangToJiaos;
  frmselyuangmutil.Execute_dll_showmodal(@dllparams);
  FreeAndNil(frmselyuangmutil);
  
  //刷新角色下员工
  refreshdata_jiaosyuang(jiaosbm);
  }
end;

procedure Tfrmxitgl_quanxgl.refreshdata_jiaosyuang(jiaosbm: integer);
begin
  qryjiaosyg.OpenSql(format('select * from v_yuang ' +
    ' where jiaosbm=%d', [jiaosbm]));
end;

procedure Tfrmxitgl_quanxgl.addYuangToJiaos(yuangbm: integer);
var
  jiaosbm: integer;
begin
  jiaosbm:= qryjiaosdy.getInteger('bianm');
  if jiaosbm<=0 then Exit;
  
  //$$$检查员工旧角色
  qry1.ExecuteSql(format('delete from sys_yuangjs' +
    ' where yuangbm=%d',
    [yuangbm]));

  //新增员工角色定义
  qry1.ExecuteSql(format('insert into sys_yuangjs(yuangbm,jiaosbm)' +
    ' values(%d,%d)',
    [yuangbm, jiaosbm]));

  //员工表角色编码字段维护
  qry1.ExecuteSql(format('update jc_yuang set jiaosbm=%d' +
    ' where bianm=%d',
    [jiaosbm, yuangbm]));  
end;

end.
