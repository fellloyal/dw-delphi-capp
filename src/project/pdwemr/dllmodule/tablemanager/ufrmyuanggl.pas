unit ufrmyuanggl;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ufrmdbeditbaseV3, ADODB, UpAdoStoreProc, frxExportXLS, frxClass,
  frxExportPDF, Menus, UpPopupMenu, frxDesgn, frxDBSet, UpDataSource, DB,
  UpAdoTable, UpAdoQuery, Grids, Wwdbigrd, Wwdbgrid, UpWWDbGrid, UPanel,
  ExtCtrls, Buttons, UpSpeedButton, ComCtrls, UpTreeView, ImgList,
  UpSplitter, StdCtrls, UpEdit, UpLabel, UpCheckBox, udbtree, udmxiangm,
  utypes, UpComboBox, ToolPanels, UpAdvToolPanel, RzButton, RzPanel, hzspell,
  ufrmdbeditbaseV2, Mask, RzEdit, RzSpnEdt, UpRzspinedit, udmtblbase;

type
  tfrmyuanggl = class(TfrmdbeditbaseV3)
    ilimglisttree: TImageList;
    qryxiangmtree: TUpAdoQuery;
    ilimglisttree111: TImageList;
    upspltr1: TUpSplitter;
    p1: TUpAdvToolPanel;
    tvxiangm: TUpTreeView;
    lbl1: TUpLabel;
    edtseljianp: TUpEdit;
    lbl2: TUpLabel;
    edtdenglzh: TUpEdit;
    lbl3: TUpLabel;
    edtxingm: TUpEdit;
    lbl5: TUpLabel;
    cbbxingb: TUpComboBox;
    lbl4: TUpLabel;
    edtnianl: TUpEdit;
    cbbyuangjiaos: TUpComboBox;
    lbl7: TUpLabel;
    edtbum: TUpEdit;
    lbl6: TUpLabel;
    lbl8: TUpLabel;
    edtpaixh: tuprzspinedit;
    btnselect: TUpSpeedButton;
    chksuojpscjqx: TCheckBox;
    chkbumpsshqx: TCheckBox;
    chkbumpscjqx: TCheckBox;
    chkbuxsjyyg: TUpCheckBox;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure tvxiangmChange(Sender: TObject; Node: TTreeNode);
    procedure tvxiangmDblClick(Sender: TObject);
    procedure tvxiangmGetImageIndex(Sender: TObject; Node: TTreeNode);
    procedure dbgCalcCellColors(Sender: TObject; Field: TField;
      State: TGridDrawState; Highlight: Boolean; AFont: TFont;
      ABrush: TBrush);
    procedure btnselectClick(Sender: TObject);
    procedure edtseljianpKeyPress(Sender: TObject; var Key: Char);
  private
    strsql : string;
      
    dbtree : TDBTree;    
    //目录树操作对象

  private
    function getnodename_showtreenode() : string;
    //显示目录树获取节点显示名称回调函数

    procedure xiangmtree_refresh();
    //刷新项目树
    procedure xiangm_refresh();
    //过滤项目dbg
  protected
    function buildselectsql() : boolean; override;
    //构造查询筛选语句
    procedure EditControlInit; override;

    function AppendBefore() : Boolean; override;
    function AppendCheck() : Boolean; override;
    procedure Append_InParams(); override;

    function UpdateBefore() : boolean; override;
    procedure Update_InParams(); override;
    
  public
    function Execute_dll_show_before() : Boolean; override;
  end;

var
  frmyuanggl: tfrmyuanggl;

implementation

uses ufrmbum, ufrmdbbase;

{$R *.dfm}

procedure tfrmyuanggl.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;
  
  frmyuanggl := nil;
end;

function tfrmyuanggl.AppendBefore: Boolean;
begin
  inherited AppendBefore; 

  Result := False;

  //编辑控件初始化
  EditControlInit;
                    
  //将类别的属性复制到新增项目
  //分类信息
  edtbum.Text := dbtree.getnodefullname(qryxiangmtree.fieldbyname('bianm').AsInteger);
  edtbum.StrValue := qryxiangmtree.fieldbyname('bianm').AsString;
  //2012-7-23：在分类编码全名结尾添加一个/符号,检索过滤用;
  edtbum.StrValue2 := dbtree.getnodefullbm(qryxiangmtree.fieldbyname('bianm').AsInteger) + '/';

  edtdenglzh.SetFocus;
  
  Result := True;
end;

function tfrmyuanggl.AppendCheck: Boolean;
begin
  Result := False;

  //登录账号不得为空
  if trim(edtdenglzh.Text)='' then
  begin
    baseobj.showwarning('账号不得为空！');
    edtdenglzh.SetFocus;
    exit;    
  end;

  //用户名称不得为空
  if Trim(edtxingm.Text) = '' then
  begin
    baseobj.showerror('姓名不得为空！');
    edtxingm.SetFocus;
    Exit;
  end;

  //部门不得为空
  if Trim(edtbum.Text) = '' then
  begin
    baseobj.showerror('类别不得为空,请双击左边类别树选择项目类别！');
    Exit;
  end;

  {
  //性别必须选择
  if cbbxingb.ItemIndex = -1 then
  begin
    baseobj.showerror('请选择性别！');
    cbbxingb.SetFocus;
    Exit;
  end;

  //年龄非法
  if not baseobj.strisint(edtnianl.Text) then
  begin
    baseobj.showerror('非法的年龄！');
    edtnianl.SetFocus;
    Exit;
  end;
  }

  //员工角色必须选择
  if cbbyuangjiaos.ItemIndex = -1 then
  begin
    baseobj.showerror('员工角色必须选择！');
    cbbyuangjiaos.SetFocus;
    exit;
  end;

  Result := True;
end;

procedure tfrmyuanggl.EditControlInit;
begin
  inherited;

  //名称
  edtxingm.Text := '';
  edtdenglzh.Text := '';
  edtnianl.Text := '';
  cbbxingb.ItemIndex := 0;
  edtbum.Text := '';
  edtbum.StrValue := '';
  edtbum.StrValue2 := '';
end;

function tfrmyuanggl.Execute_dll_show_before: Boolean;
begin             
  inherited Execute_dll_show_before;

  FDmBase := Tdmtblbase.Create(@dllparams);
  FDmBase.appendSpName := 'tbl_ls_yuang_append';
  FDmBase.updateSpName := 'tbl_ls_yuang_update';
  FDmBase.deleteSpName := 'tbl_ls_yuang_delete';
  FTableCnName := '员工';

  { 性别
  }
  cbbxingb.ClearItems;
  cbbxingb.AddItemUP('男','1');
  cbbxingb.AddItemUP('女','2');

  { 员工角色
  }
  cbbyuangjiaos.ClearItems;
  dmtbl_public.fillcbb_userTableName(@cbbyuangjiaos, 'sys_jiaosdy');

  { 初始化项目分类树
  }
  //显示项目树
  xiangmtree_refresh();
  
  { 填充编辑控件
  }

  qry.SQL.Text := 'select * from v_yuang where 1=2';
  RefreshData;  

  Result := True;
end;

function tfrmyuanggl.getnodename_showtreenode: string;
begin
  Result := qryxiangmtree.fieldbyname('yonghbm').AsString + ' ' +
      qryxiangmtree.fieldbyname('mingc').AsString;
end;

function tfrmyuanggl.UpdateBefore: boolean;
begin                   
  inherited UpdateBefore;

  //填充当前商品信息到编辑框
  edtdenglzh.Text := qry.fieldbyname('denglzh').AsString;
  edtxingm.Text := qry.fieldbyname('xingm').AsString;
  edtnianl.Text := qry.fieldbyname('nianl').AsString;
  cbbxingb.ItemIndex := cbbxingb.ValueItemIndex(qry.getString('xingb'));
  edtbum.Text := qry.fieldbyname('bummcqm').AsString;
  edtbum.StrValue := qry.fieldbyname('bumbm').AsString;
  edtbum.StrValue2 := qry.fieldbyname('bumbmqm').AsString;
  cbbyuangjiaos.ItemIndex := cbbyuangjiaos.ValueItemIndex(qry.getString('jiaosbm'));
  edtpaixh.Text := qry.getString('paixh');
  //所级创建权限
  chksuojpscjqx.Checked := qry.getInteger('suojpscjqx') = 1;
  //部门创建权限
  chkbumpscjqx.Checked := qry.getInteger('bumpscjqx') = 1;
  //部门审核权限
  chkbumpsshqx.Checked := qry.getInteger('bumpsshqx') = 1;

  Result := True;
end;

procedure tfrmyuanggl.xiangmtree_refresh;
begin
  if not Assigned(dbtree) then
  begin
    //创建树操作对象
    dbtree := TDBTree.Create(qryxiangmtree, 'jc_bum', tvxiangm, dllparams.padoconn);
    dbtree.fgetnodename_showtreenode := getnodename_showtreenode;
  end;

  dbtree.wherestr:= ' where zhuangt=1 ';
  //显示树
  dbtree.showtree;
end;

procedure tfrmyuanggl.tvxiangmChange(Sender: TObject;
  Node: TTreeNode);
begin
  inherited;

  //树当前选中节点改变,通知qry改变当前记录
  dbtree.treechange(node);

  xiangm_refresh;
end;

procedure tfrmyuanggl.tvxiangmDblClick(Sender: TObject);
begin
  inherited;

  { 在编辑状态下修改编辑控件分类信息
  }
  if frmeditstat <> fesBrower then
  begin
    //是否选中分类
    if tvxiangm.Selected <> nil then
    begin
      //分类
      edtbum.Text := dbtree.getnodefullname(qryxiangmtree.fieldbyname('bianm').AsInteger);
      edtbum.StrValue := qryxiangmtree.fieldbyname('bianm').AsString;
      //2012-7-23：在分类编码全名结尾添加一个/符号,检索过滤用;
      edtbum.StrValue2 := dbtree.getnodefullbm(qryxiangmtree.fieldbyname('bianm').AsInteger) + '/';
    end
    else
    begin
      edtbum.Text := '';
      edtbum.StrValue := '';
      edtbum.StrValue2 := '';
    end;
  end;
end;

procedure tfrmyuanggl.tvxiangmGetImageIndex(Sender: TObject;
  Node: TTreeNode);
begin
  inherited;

  { 改变节点图标
  }
  if Node.Selected then
  begin
    Node.ImageIndex := 1;
  end
  else
  begin
    Node.ImageIndex := 2;
  end;
end;

procedure tfrmyuanggl.dbgCalcCellColors(Sender: TObject;
  Field: TField; State: TGridDrawState; Highlight: Boolean; AFont: TFont;
  ABrush: TBrush);
begin
  inherited;

  case Field.DataSet.FieldByName('zhuangt').AsInteger of
    1:
    begin
      AFont.Color := clBlack;
    end;
    0:
    begin
      AFont.Color := clRed;
    end;
  end;
end;

function tfrmyuanggl.buildselectsql: boolean;
begin
  result := true;
end;

procedure tfrmyuanggl.Append_InParams;
begin
  inherited;

  FDmBase.addspparam_in('@xingm', edtxingm.Text);
  FDmBase.addspparam_in('@jianp', THzSpell.PyHeadOfHz(edtxingm.Text));
  FDmBase.addspparam_in('@bumbm', edtbum.StrValue);
  if cbbxingb.ItemIndex=-1 then
    FDmBase.addspparam_in('@xingb', '1')
  else
    FDmBase.addspparam_in('@xingb', cbbxingb.GetItemValue);
  FDmBase.addspparam_in('@nianl', inttostr(strtointdef(edtnianl.Text,0)));
  FDmBase.addspparam_in('@denglzh', edtdenglzh.Text);
  FDmBase.addspparam_in('@koul', '123456');
  FDmBase.addspparam_in('@jiaosbm', cbbyuangjiaos.GetItemValue);
  FDmBase.addspparam_in('@paixh', edtpaixh.Value);
  //所级评审创建权限
  if chksuojpscjqx.Checked then
    FDmBase.addspparam_in('@suojpscjqx', 1)
  else
    FDmBase.addspparam_in('@suojpscjqx', 0);
  //部门评审创建权限
  if chkbumpscjqx.Checked then
    FDmBase.addspparam_in('@bumpscjqx', 1)
  else
    FDmBase.addspparam_in('@bumpscjqx', 0);
  //部门评审审核权限
  if chkbumpsshqx.Checked then
    FDmBase.addspparam_in('@bumpsshqx', 1)
  else
    FDmBase.addspparam_in('@bumpsshqx', 0);
end;

procedure tfrmyuanggl.Update_InParams;
begin
  inherited;
  
  Append_InParams();
end;

procedure tfrmyuanggl.xiangm_refresh;
begin
  if tvxiangm.Selected = nil then
  begin
    qry.OpenSql('select * from v_yuang where 1=2')
  end
  else
  begin
    if chkbuxsjyyg.Checked then
    begin   
      qry.OpenSql(format('select * from v_yuang' +
        ' where bumbm=%d' +
        '   and zhuangt=1' +
        ' order by paixh',
        [qryxiangmtree.getInteger('bianm')]));

    end
    else
    begin  
      qry.OpenSql(format('select * from v_yuang' +
        ' where bumbm=%d' +
        ' order by paixh',
        [qryxiangmtree.getInteger('bianm')]));

    end;
  end;
end;

procedure tfrmyuanggl.btnselectClick(Sender: TObject);
begin
  inherited;

  if trim(edtseljianp.Text)<>'' then
  begin
    qry.OpenSql(format('select * from v_yuang' +
      ' where denglzh like ''%s%s%s''' +
      ' order by paixh',
       ['%', edtseljianp.Text, '%']));
  end
  else
  begin
    qry.OpenSql('select * from v_yuang where 1=2');
  end;
end;

procedure tfrmyuanggl.edtseljianpKeyPress(Sender: TObject; var Key: Char);
begin
  inherited;

  if key=chr(13) then
    btnselectClick(btnselect);
end;

end.
