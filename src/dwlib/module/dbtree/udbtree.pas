//*******************************************************************
//  pmyhisgroup  ptblmanage   
//  udbtree.pas     
//                                          
//  创建: changym by 2012-02-21 
//        changup@qq.com                    
//  功能说明:                                            
//      目录树操作类;
//      2012-2-21: 提供展示树的功能;
//  修改历史:
//                                            
//  版权所有 (C) 2012 by changym
//                                                       
//*******************************************************************

unit udbtree;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, ComCtrls, DB, ADODB,
  ubase, udbcheck, utypes;

type
  TFunGetNodeName_ShowTreeNode = function() : string of object;
  
  TDBTree = class(tbase)
  private
    qrytmp1 : TADOQuery;
    //自由query

    padoconn : PUpAdoConnection;
    //数据库连接对象 

    querytree : TADOQuery;
    //tree的数据集对象
    tablename : string;
    //表名
    tvtree : TTreeView;
    //tree的控件对象

    nodelist : TStringList;
    //节点辅助存储，存储各行的书签
    //key=节点编码，value=节点对象TreeNode的实例
  private
    procedure showtreenode();
    //显示querytree当前节点到树控件
  public
    constructor Create(qrytree : TADOQuery; tblname : string; tv: TTreeView; conn : PUpAdoConnection);
    destructor Destroy(); override;
  public
    procedure showtree();
    //根据querytree显示树控件

    function getnodefullbm(nodeid : Integer) : string;
    //得到节点完整编码路径
    function getnodefullname(nodeid : integer) : string;
    //得到节点完整名称，追朔到根节点的名称

    procedure treechange(node : TTreeNode);
    //树控件当前节点改变，通知querytree移动当前记录

    procedure gotoNode(bianm : integer);
    //定位到指定的编码节点
  public
    fgetnodename_showtreenode : TFunGetNodeName_ShowTreeNode;
    //获取显示节点时的节点名称函数指针,已方便不同类型目录树的自定义显示名称
    
    orderby : string; //order by 语句
    wherestr : string; //条件语句
  end;

implementation

{ TDBTree }

procedure TDBTree.showtreenode;
var
  index: integer;
  Node: TTreeNode;
begin
  //根据是否是根节点不同处理
  if querytree.FieldByName('fujdbm').AsInteger = 0 then
  begin
    //根节点，增加节点，并将本节点所对应的记录标签数据放到节点所提供的附加数据中
    if Assigned(fgetnodename_showtreenode) then
    begin
      Node := tvtree.Items.AddChildObject(nil,
        fgetnodename_showtreenode, querytree.GetBookmark);
    end
    else
    begin  
      Node := tvtree.Items.AddChildObject(nil,
        querytree.FieldByName('mingc').AsString, querytree.GetBookmark);
    end;
  end
  else
  begin
    //找到父节点的node对象
    Index := nodelist.IndexOf(querytree.FieldByName('fujdbm').AsString);

    //有时候数据损坏导致父节点不存在,防止程序报错
    if index = -1 then Exit;

    
    //根节点，增加节点，并将本节点所对应的记录标签数据放到节点所提供的附加数据中
    if Assigned(fgetnodename_showtreenode) then
    begin
      Node := tvtree.Items.AddChildObject(TTreeNode(nodelist.Objects[Index]),
           fgetnodename_showtreenode, querytree.GetBookmark);
      //增加子节点，并将本节点所对应的记录标签数据放到节点所提供的附加数据中
    end
    else
    begin
      Node := tvtree.Items.AddChildObject(TTreeNode(nodelist.Objects[Index]),
           querytree.FieldByName('mingc').AsString, querytree.GetBookmark);
      //增加子节点，并将本节点所对应的记录标签数据放到节点所提供的附加数据中
    end;
  end;
  
  //增加当前节点的信息到列表中，以实现在列表中快速查找节点的功能。
  nodelist.AddObject(querytree.FieldByName('bianm').AsString, Node);
end;

constructor TDBTree.Create(qrytree : TADOQuery; tblname : string; tv: TTreeView; conn : PUpAdoConnection);
begin
  fgetnodename_showtreenode := nil;
  
  //数据库连接对象
  padoconn := conn;

  //存储树相关对象
  querytree := qrytree;
  tablename := tblname;
  tvtree := tv;

  //创建树辅助存储对象
  nodelist := TStringList.Create;

  //创建自由query对象
  qrytmp1 := TADOQuery.Create(nil);
  qrytmp1.Connection := querytree.Connection;
end;

destructor TDBTree.Destroy;
begin
  //销毁树辅助存储对象
  FreeAndNil(nodelist);

  //销毁自由query                    
  qrytmp1.Free;
  qrytmp1 := nil;
end;

procedure TDBTree.showtree;
begin
  tvtree.Items.BeginUpdate;

  //情况树形控件和辅助记录列表
  nodelist.Clear;
  tvtree.items.clear;

  //查询树信息
  if querytree.Active then querytree.Close;
  if orderby='' then
  begin
    orderby := ' order by fujdbm,paixh,jianp';
  end;
  querytree.SQL.Text := 'SELECT * FROM ' + tablename +
    ' ' + wherestr + ' ' + orderby;
  querytree.Open;

  querytree.DisableControls;
  nodelist.Sorted := True;
  querytree.First;
  while not querytree.Eof do
  begin
    //显示当前的节点
    showtreenode();
    
    querytree.Next;
  end;
  querytree.EnableControls;
  tvtree.Items.EndUpdate;
end;

procedure TDBTree.treechange(node: TTreeNode);
begin
  querytree.GotoBookmark(node.Data);
end;

function TDBTree.getnodefullname(nodeid: integer): string;
var
  qry : TADOQuery;
begin
  Result := '';

  qry := TADOQuery.Create(nil);
  qry.Connection := padoconn^;
  qry.Close;
  qry.SQL.Text := 'select fujdbm, mingc from ' + tablename + ' where bianm=' + IntToStr(nodeid);
  qry.Open;
  if qry.FieldByName('fujdbm').AsInteger <> 0 then
  begin
    Result := getnodefullname(qry.fieldbyname('fujdbm').AsInteger) +
       '/' + qry.fieldbyname('mingc').AsString + result;
  end
  else
  begin
    Result := '/' + qry.fieldbyname('mingc').AsString;
  end;
  qry.Close;
  FreeAndNil(qry);
end;

function TDBTree.getnodefullbm(nodeid: Integer): string;
var
  qry : TADOQuery;
begin
  Result := '';

  qry := TADOQuery.Create(nil);
  qry.Connection := padoconn^;
  qry.Close;
  qry.SQL.Text := 'select bianm, fujdbm from ' + tablename + ' where bianm=' + IntToStr(nodeid);
  qry.Open;
  if qry.FieldByName('fujdbm').AsInteger <> 0 then
  begin
    Result := getnodefullbm(qry.fieldbyname('fujdbm').AsInteger) +
       '/' + qry.fieldbyname('bianm').AsString + result;
  end
  else
  begin
    Result := '/' + qry.fieldbyname('bianm').AsString;
  end;
  qry.Close;
  FreeAndNil(qry);
end;

procedure TDBTree.gotoNode(bianm: integer);
var
  i: integer;
  Node: TTreeNode;
  strbianm : string;
begin
  strbianm := IntToStr(bianm);

  for i:=0 to nodelist.Count-1 do
  begin
    if nodelist.Strings[i] = strbianm then
    begin
      Node := TTreeNode(nodelist.Objects[i]);
      tvtree.Selected := Node;
      
      break;
    end;
  end;       
end;

end.

