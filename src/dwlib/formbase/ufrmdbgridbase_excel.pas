{ 带从Excel导入的dngridebase基类；
  add by changym as 2014-07-16
  for 甘肃新农合实时结算接口；
}
unit ufrmdbgridbase_excel;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ADODB, UpAdoStoreProc, frxExportXLS, frxClass,
  frxExportPDF, Menus, UpPopupMenu, frxDesgn, frxDBSet, UpDataSource, DB,
  UpAdoTable, UpAdoQuery, Grids, Wwdbigrd, Wwdbgrid, UpWWDbGrid, ExtCtrls,
  Buttons, UpSpeedButton, ufrmimportfromexcel, utypes, StdCtrls, UpEdit,
  UpLabel, UPanel, ufrmmdichildbase;

type
  Tfrmdbgridbase_excel = class(Tfrmmdichildbase)
    upnlpselect: TUPanel;
    lbl1: TUpLabel;
    edtseljianp: TUpEdit;
    pnlpbuttons: TPanel;
    btnquit: TUpSpeedButton;
    bvl1: TBevel;
    btnclear: TUpSpeedButton;
    btnimport: TUpSpeedButton;
    dbg: TUpWWDbGrid;
    procedure btnclearClick(Sender: TObject);
    procedure btnimportClick(Sender: TObject);
    procedure edtseljianpKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure btnquitClick(Sender: TObject);
  private

  protected
    procedure refreshDataSet(); virtual; abstract;
    //刷新数据集
    
    //数据清空时处理函数；
    function truncateDataBefore():Boolean;virtual;abstract;
    function truncateData():Boolean;virtual;abstract;
    function truncateDataAfter():Boolean;virtual;abstract;
                                                          
    procedure importExcelData(pqry : PUpAdoQuery); virtual; abstract;
    function setImpdataMsg(pqry : PUpAdoQuery) : string; virtual; abstract;
    //设置提示信息；在打开页面后统计并显示；
    function validateDataset(pqry : PUpAdoQuery) : string; virtual; abstract;
    //数据验证；
    
    function Execute_dll_show_before() : Boolean; override;
  public
  end;

var
  frmdbgridbase_excel: Tfrmdbgridbase_excel;

implementation

{$R *.dfm}

procedure Tfrmdbgridbase_excel.btnclearClick(Sender: TObject);
begin
  inherited;

  //询问是否清空？
  if MessageDlg('确认清空当前数据吗？', mtInformation, [mbYes, mbNo], 0) <> mrYes then
  begin
    Exit;
  end;

  if not truncateDataBefore then Exit;

  if not truncateData then Exit;

  truncateDataAfter();

  //清空后刷新数据集；
  refreshDataSet();
end;

function Tfrmdbgridbase_excel.Execute_dll_show_before: Boolean;
begin
  Result := inherited Execute_dll_show_before;

  //刷新数据集;
  refreshDataSet();
end;

procedure Tfrmdbgridbase_excel.btnimportClick(Sender: TObject);
var
  frmimportexceldata : Tfrmimportexceldata;
begin
  inherited;

  frmimportexceldata := Tfrmimportexceldata.Create(nil);
  frmimportexceldata.ffun_importexceldata := importExcelData;
  frmimportexceldata.ffun_SetImpCountMsg := setImpdataMsg;
  frmimportexceldata.ffun_validataset := validateDataset;
  frmimportexceldata.Execute_dll_showmodal(@dllparams);
  FreeAndNil(frmimportexceldata);

  //刷新数据集; 这个放在这里似乎不合适，凑活这吧；2014-07-16；changym；
  refreshDataSet();
end;

procedure Tfrmdbgridbase_excel.edtseljianpKeyUp(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  inherited;

  if edtseljianp.Text='' then
  begin
    { 没有过滤条件了}
    qry.Filtered := False;
    qry.Filter := '';
    qry.Filtered := True;
  end
  else
  begin
    { 按照简拼过滤}
    qry.Filtered := False;
    qry.Filter := 'jianp like ''%' + edtseljianp.Text + '%''';
    qry.Filtered := True;
  end;
end;

procedure Tfrmdbgridbase_excel.btnquitClick(Sender: TObject);
begin
  inherited;

  close;
end;

end.
