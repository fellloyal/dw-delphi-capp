//*******************************************************************
//  ProjectGroup1  pfuzhuang   
//  uDmBase.pas     
//                                          
//  创建: changym by 2012-01-03 
//        changup@qq.com                    
//  功能说明:
//      数据库操作类的基类；方便利用Vcl本身的控件，减少诸如TAdoStoreProc
//      类控件参数的创建和管理而采用从TDataModule继承而来；
//  修改历史:
//                                            
//  版权所有 (C) 2012 by changym
//                                                       
//*******************************************************************

unit uDmBase;

interface

uses
  SysUtils, Classes, udlltransfer, ADODB, UpAdoStoreProc, DB, UpAdoQuery,
  UpAdoTable, udbcheck;

type
  TDMBase = class(TDataModule)
    qry: TUpAdoQuery;
    qry1: TUpAdoQuery;
    qry2: TUpAdoQuery;  
    qry3: TUpAdoQuery;
    sp_delete: TUpAdoStoreProc;
    sp_append: TUpAdoStoreProc;
    sp_update: TUpAdoStoreProc;
  private
  protected
    fpdllparams : PDllParam; 
    dbcheck : tdbcheck;
  public
    errcode : Integer;
    errmsg : string;
    //错误码和错误描述
  public
    procedure BeginTrans();
    procedure CommitTrans();
    procedure RollbackTrans;
                
    function SplitString(const source, ch: string): TStringList;
    //字符串分割

  public
    constructor Create(pdllparams : PDllParam); virtual;
    destructor Destroy; override;

  end;

implementation

{$R *.dfm}

{ TDMBase }

procedure TDMBase.BeginTrans;
begin
  fpdllparams^.padoconn.BeginTrans;
end;

procedure TDMBase.CommitTrans;
begin
  fpdllparams^.padoconn.CommitTrans;
end;

constructor TDMBase.Create(pdllparams : PDllParam);
var
  i : Integer;
begin
  inherited Create(nil);

  fpdllparams := pdllparams;
  
  //数据库规则检查对象
  dbcheck := tdbcheck.create(fpdllparams.padoconn);
  
  { 窗体数据集connection属性设置
  }
  for i:=0 to self.ComponentCount-1 do
  begin
    //TAdoQuery
    if UpperCase(Self.Components[i].ClassName) = 'TADOQUERY' then
    begin
      (self.Components[i] as TADOQuery).Connection := fpdllparams^.padoconn^;
    end;    
    //TUpAdoQuery
    if UpperCase(Self.Components[i].ClassName) = 'TUPADOQUERY' then
    begin
      (self.Components[i] as TUPADOQUERY).Connection := fpdllparams^.padoconn^;
    end;
    
    //TADOTABLE
    if UpperCase(Self.Components[i].ClassName) = 'TADOTABLE' then
    begin
      (self.Components[i] as TADOTABLE).Connection := fpdllparams^.padoconn^;
    end;
    //TUPADOTABLE
    if UpperCase(Self.Components[i].ClassName) = 'TUPADOTABLE' then
    begin
      (self.Components[i] as TUPADOTABLE).Connection := fpdllparams^.padoconn^;
    end;

    //TADOSTOREDPROC
    if UpperCase(self.Components[i].ClassName) = 'TADOSTOREDPROC' then
    begin
      (Self.Components[i] as TADOStoredProc).Connection := fpdllparams^.padoconn^;
    end;    
    //TUPADOSTOREPROC
    if UpperCase(self.Components[i].ClassName) = 'TUPADOSTOREPROC' then
    begin
      (Self.Components[i] as TUpAdoStoreProc).Connection := fpdllparams^.padoconn^;
    end;
  end;
end;

destructor TDMBase.Destroy;
begin
  //销毁数据库规则检查对象
  FreeAndNil(dbcheck);
  inherited;
end;

procedure TDMBase.RollbackTrans;
begin
  fpdllparams^.padoconn.RollbackTrans;
end;

function TDMBase.SplitString(const source, ch: string): TStringList;
var
  temp, t2: string;
  i: integer;
begin
  result := TStringList.Create;
  temp := source;
  i := pos(ch, source);
  while i <> 0 do
  begin
    t2 := copy(temp, 0, i - 1);
    if (t2 <> '') then
      result.Add(t2);
    delete(temp, 1, i - 1 + Length(ch));
    i := pos(ch, temp);
  end;
  result.Add(temp);
end;

end.
